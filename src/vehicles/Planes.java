package vehicles;
/**
 *
 * @author Jose
 */
public class Planes extends Vehicle {
    
    private boolean isFlying;
    
    public Planes(String make, String model, String year, String weight) 
    {
        super(make, model, year, weight);
        this.isFlying = false;
    }

    public boolean getIsFlying() {
        return isFlying;
    }

    public void setIsFlying(boolean isFlying) {
        this.isFlying = isFlying;
    }
    
    public boolean fly()
    {
        if(getNeedsMaintenance())
        {
            System.out.println("The plane cannot fly until it's fully repaired");
            return false;
        }
        else
            if (!getIsFlying()) setIsFlying(true);
        return true;
    }
    
    public void land()
    {
        if (getIsFlying())
        {
            setIsFlying(false);
            setTripsSinceMaintenance(getTripsSinceMaintenance()+ 1);
            if(getTripsSinceMaintenance()== 100) setNeedsMaintenance(true);
        }
    }
    
}
