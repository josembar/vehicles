package vehicles;
/**
 *
 * @author Jose
 */
public class Cars extends Vehicle {
    
    private boolean isDriving;
    
    public Cars(String make, String model, String year, String weight) 
    {
        super(make, model, year, weight);
        this.isDriving = false;
    }

    public boolean getIsDriving() {
        return isDriving;
    }

    public void setIsDriving(boolean isDriving) {
        this.isDriving = isDriving;
    }
    
    public void drive()
    {
        if (!getIsDriving()) setIsDriving(true);
    }
    
    public void stop()
    {
        if (getIsDriving())
        {
            setIsDriving(false);
            setTripsSinceMaintenance(getTripsSinceMaintenance()+ 1);
            if(getTripsSinceMaintenance()== 100) setNeedsMaintenance(true);
        }
    }
    
    public void repair()
    {
        setTripsSinceMaintenance(0);
        setNeedsMaintenance(false);
    }
}
