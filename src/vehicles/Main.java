package vehicles;
/**
 *
 * @author Jose
 */
public class Main {

    public static void main(String[] args) {
        
        //for loops created to simulate car behaviour
        Cars c1 = new Cars("Toyota", "Camry", "2010", "1000 pounds");
        for(int i = 1; i <= 100; i++)
        {
            c1.drive();
            c1.stop();
        }
        
        Cars c2 = new Cars("Honda", "Civic", "2015", "800 pounds");
        for(int i = 1; i <= 4; i++)
        {
            c2.drive();
            c2.stop();
        }
        
        Cars c3 = new Cars("Volkswagen", "Polo", "2012", "900 pounds");
        for(int i = 1; i <= 3; i++)
        {
            c3.drive();
            c3.stop();
        }
        
        System.out.println("Printing car 1 info...");
        System.out.println("Make: " + c1.getMake());
        System.out.println("Model: " + c1.getModel());
        System.out.println("Year: " + c1.getYear());
        System.out.println("Weight: " + c1.getWeight());
        System.out.println("Does it needs maintenance?: " + c1.getNeedsMaintenance());
        System.out.println("Trips since maintenance: " + c1.getTripsSinceMaintenance());
        System.out.println();
        
        System.out.println("Printing car 2 info...");
        System.out.println("Make: " + c2.getMake());
        System.out.println("Model: " + c2.getModel());
        System.out.println("Year: " + c2.getYear());
        System.out.println("Weight: " + c2.getWeight());
        System.out.println("Does it needs maintenance?: " + c2.getNeedsMaintenance());
        System.out.println("Trips since maintenance: " + c2.getTripsSinceMaintenance());
        System.out.println();
        
        System.out.println("Printing car 3 info...");
        System.out.println("Make: " + c3.getMake());
        System.out.println("Model: " + c3.getModel());
        System.out.println("Year: " + c3.getYear());
        System.out.println("Weight: " + c3.getWeight());
        System.out.println("Does it needs maintenance?: " + c3.getNeedsMaintenance());
        System.out.println("Trips since maintenance: " + c3.getTripsSinceMaintenance());
        System.out.println();
        
        System.out.println("Repairing car 1...");
        c1.repair();
        if(!c1.getNeedsMaintenance())
        {
            System.out.println("Does it needs maintenance?: " + c1.getNeedsMaintenance());
            System.out.println("Trips since maintenance: " + c1.getTripsSinceMaintenance());
        }
        System.out.println();
       
        //for loops created to simulate plane behaviour
        Planes p1 = new Planes("Airbus", "A220", "2010", "10000 pounds");
        for(int i = 1; i <= 100; i++)
        {
            p1.fly();
            p1.land();
        }
        
        Planes p2 = new Planes("Boeing", "777", "2013", "12000 pounds");
        for(int i = 1; i <= 3; i++)
        {
            p2.fly();
            p2.land();
        }
             
        System.out.println("Printing plane 1 info...");
        System.out.println("Make: " + p1.getMake());
        System.out.println("Model: " + p1.getModel());
        System.out.println("Year: " + p1.getYear());
        System.out.println("Weight: " + p1.getWeight());
        System.out.println("Does it needs maintenance?: " + p1.getNeedsMaintenance());
        System.out.println("Trips since maintenance: " + p1.getTripsSinceMaintenance());
        System.out.println();
        
        System.out.println("Printing plane 2 info...");
        System.out.println("Make: " + p2.getMake());
        System.out.println("Model: " + p2.getModel());
        System.out.println("Year: " + p2.getYear());
        System.out.println("Weight: " + p2.getWeight());
        System.out.println("Does it needs maintenance?: " + p2.getNeedsMaintenance());
        System.out.println("Trips since maintenance: " + p2.getTripsSinceMaintenance());
        System.out.println();
        
        System.out.println("Trying to fly with plane 1...");
        p1.fly();
    }
    
}
